package main

import (
	"bufio"
	"bytes"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	//"log"
	"os"
	//"runtime/pprof"
)

/*
#cgo CFLAGS: -std=c99

#include <stdio.h>
#include <string.h>

void dotPlot
(const char* seq1, const char* seq2, unsigned long windowSize, unsigned long threshold) {
	size_t len1 = strlen(seq1);
	size_t len2 = strlen(seq2);

	if (windowSize < 1) {
		fprintf(stderr, "Error: invalid window size: %ld\n", windowSize);
		return;
	}
	if (windowSize > len1 || windowSize > len2) {
		fprintf(stderr, "Error: window size greater than sequence size.\n");
		return;
	}
	if (threshold > windowSize) {
		fprintf(stderr, "Error: invalid threshold: %ld\n", threshold);
		return;
	}
	for (unsigned long i = 0; i < len1 - windowSize + 1; i++) {
		for (unsigned long j = 0; j < len2 - windowSize + 1; j++) {
			unsigned long similarity = 0;
			for (unsigned long chr = 0; chr < windowSize; chr++) {
				if (seq1[i + chr] == seq2[j + chr]) {
					similarity++;
				}
			}
			if (similarity >= threshold) {
				fprintf(stdout, "%ld %ld\n", i, j);
			}
		}
	}
	fflush(stdout); // ensures everything is printed
}
*/
import "C"

func DotPlot(seq1, seq2 string, windowSize, threshold int) error {
	if windowSize < 1 {
		return errors.New(fmt.Sprintf("Error: invalid window size: %d", windowSize))
	}
	if windowSize > len(seq1) || windowSize > len(seq2) {
		return errors.New("Error: window size greater than sequence size.")
	}
	if threshold > windowSize {
		return errors.New(fmt.Sprintf("Error: invalid threshold: %d", threshold))
	}

	// i, j as start of the window
	for i := 0; i < len(seq1)-windowSize + 1; i++ {
		for j := 0; j < len(seq2)-windowSize + 1; j++ {
			// compare the sequence segments
			similarity := 0
			chunk1 := seq1[i : i+windowSize]
			chunk2 := seq2[j : j+windowSize]
			for char := 0; char < windowSize; char++ {
				if chunk1[char] == chunk2[char] {
					similarity++
				}
			}
			if similarity >= threshold {
				fmt.Println(i, j)
			}
		}
	}
	return nil
}

func usage() {
	os.Stderr.WriteString("Usage: dotplot [OPTION]... FILE1 FILE2\n")
	os.Stderr.WriteString("Given two files, print dot plot points in format x y to stdout.\n")
	flag.PrintDefaults()
}

// Returns the sequence from a FASTA file without comments and newlines.
func readFasta(filename string) (string, error) {
	buffer := bytes.NewBufferString("")
	file, err := os.Open(filename)
	if err != nil {
		return "", err
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if line != "" && line[0] != '>' { // discard comments
			buffer.WriteString(line)
		}
	}

	if err := scanner.Err(); err != nil {
		return "", err
	}
	return buffer.String(), nil
}

//prof
//var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Usage = usage
	windowSize := flag.Int("w", 10, "window size")
	threshold := flag.Int("t", 7, "threshold")
	safe := flag.Bool("s", false, "safe mode (slower)")
	fasta := flag.Bool("f", true, "treat input files as FASTA files")
	flag.Parse()

	//prof
	/*
		if *cpuprofile != "" {
			f, err := os.Create(*cpuprofile)
			if err != nil {
				log.Fatal(err)
			}
			pprof.StartCPUProfile(f)
			defer pprof.StopCPUProfile()
		}
	*/

	if len(flag.Args()) != 2 {
		os.Stderr.WriteString("Arguments are required to be 2 files.\n")
		return
	}
	
	var seq1, seq2 string
	var err error
	
	if *fasta {
		seq1, err = readFasta(flag.Args()[0])
		if err != nil {
			os.Stderr.WriteString(err.Error() + "\n")
			return
		}
		seq2, err = readFasta(flag.Args()[1])
		if err != nil {
			os.Stderr.WriteString(err.Error() + "\n")
			return
		}
	} else {
		seq1bytes, err := ioutil.ReadFile(flag.Args()[0])
		if err != nil {
			os.Stderr.WriteString(err.Error() + "\n")
			return
		}
		seq1 = string(seq1bytes)
		seq2bytes, err := ioutil.ReadFile(flag.Args()[1])
		if err != nil {
			os.Stderr.WriteString(err.Error() + "\n")
			return
		}
		seq2 = string(seq2bytes)
	}	

	if *safe {
		err := DotPlot(seq1, seq2, *windowSize, *threshold)
		if err != nil {
			os.Stderr.WriteString(err.Error() + "\n")
		}
	} else {
		C.dotPlot(C.CString(seq1), C.CString(seq2), C.ulong(*windowSize), C.ulong(*threshold))
	}

}
