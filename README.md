A simple Go(lang) implementation of a dot plot maker used to graphically examine sequence alignment. The output is sent to stdout as x, y points, which can be used with gnuplot like so:  
	`./dotplot s1.txt s2.txt | gnuplot -p -e "plot '<cat' with points"`  

producing something like this:  
![](http://i.imgur.com/JboyEzw.png)
